package pl.drednot.memory.palace.api.exceptions;

import org.springframework.http.HttpStatus;

public class MemoryNotFound extends APICommonException {

    public MemoryNotFound(String memoryId){
        super(HttpStatus.NOT_FOUND, "Memory with id = " + memoryId + " not found");
    }

}
