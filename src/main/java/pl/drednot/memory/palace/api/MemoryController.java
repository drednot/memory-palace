package pl.drednot.memory.palace.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.drednot.memory.palace.api.exceptions.MemoryNotFound;
import pl.drednot.memory.palace.data.Memory;
import pl.drednot.memory.palace.services.MemoryService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/memories")
public class MemoryController {

    private static final Logger LOG = LoggerFactory.getLogger(MemoryController.class);

    @Autowired private MemoryService memoryService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Memory> getMemories() {
        LOG.debug("Get memories list");
        return memoryService.getAll();
    }

    @GetMapping(value = "/hierarchy", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Memory> getMemoriesHierarchy() {
        LOG.debug("Get memories hierarchy");
        return memoryService.getHierarchy();
    }

    @GetMapping(value = "/{memoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Memory getMemory(@PathVariable("memoryId") UUID memoryId){
        LOG.debug("Get memory with id {}.", memoryId);
        Memory memory = memoryService.get(memoryId);
        if(memory != null){
            return memory;
        }
        throw new MemoryNotFound(memoryId.toString());
    }

    @PutMapping(value = "/{memoryId}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Memory updateMemory(@RequestBody Memory memory){
        LOG.debug("Update memory: " + memory);
        return memoryService.update(memory);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Memory createMemory(@RequestBody Memory memory){
        LOG.debug("Create new memory: " + memory);
        return memoryService.create(memory);
    }

    @DeleteMapping(value = "/{memoryId}")
    public void deleteMemory(@PathVariable("memoryId") UUID memoryId){
        LOG.debug("Delete memory with id = {}.", memoryId);
        memoryService.delete(memoryId);
    }

}
