package pl.drednot.memory.palace.api.exceptions;

import org.springframework.http.HttpStatus;

public class APICommonException extends RuntimeException {

    private HttpStatus status;

    public APICommonException(HttpStatus status, String message){
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
