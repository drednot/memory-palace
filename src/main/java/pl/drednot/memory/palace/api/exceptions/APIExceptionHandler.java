package pl.drednot.memory.palace.api.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class APIExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(APICommonException.class)
    public ResponseEntity<ExceptionResponse> exceptionHandler(APICommonException exception) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(exception.getClass().getSimpleName(), exception.getMessage());
        return new ResponseEntity<>(exceptionResponse, exception.getStatus());
    }

    private class ExceptionResponse {
        String error;
        String message;

        ExceptionResponse(String error, String message) {
            this.error = error;
            this.message = message;
        }
    }


}
