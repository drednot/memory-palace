package pl.drednot.memory.palace.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Memory {

    @Id
    public UUID id;
    public UUID parentId;
    public String name;
    public int index;
    public List<MemorySection> sections = new ArrayList<>();

    @Transient
    public List<Memory> memories = new ArrayList<>();

    public Memory() {
    }

    public Memory(UUID id, UUID parentId) {
        this.id = id;
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Memory memory = (Memory) o;

        return id.equals(memory.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Memory{" +
                "id=" + id +
                ", parentId=" + parentId +
                ", name='" + name + '\'' +
                ", index=" + index +
                ", sections=" + sections +
                ", memories=" + memories +
                '}';
    }
}
