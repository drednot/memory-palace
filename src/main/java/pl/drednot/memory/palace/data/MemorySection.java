package pl.drednot.memory.palace.data;

import java.util.List;

public class MemorySection {

    public MemorySectionType type;
    public String value;
    public List<MemorySectionElement> elements;

}
