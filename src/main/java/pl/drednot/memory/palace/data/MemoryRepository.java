package pl.drednot.memory.palace.data;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.UUID;

public interface MemoryRepository extends MongoRepository<Memory, UUID> {

    List<Memory> findAllByParentId(UUID parentId, Sort sort);

    List<Memory> findAllByParentIdAndIndexGreaterThanEqual(UUID parentId, int index, Sort sort);

}
