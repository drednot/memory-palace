package pl.drednot.memory.palace.data;

public enum MemorySectionType {

    paragraph,
    list,
    console

}
