package pl.drednot.memory.palace.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import pl.drednot.memory.palace.data.Memory;
import pl.drednot.memory.palace.data.MemoryRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class MemoryService {

    private MemoryRepository repository;

    @Autowired
    public MemoryService(MemoryRepository repository) {
        this.repository = repository;
    }

    public List<Memory> getHierarchy() {
        List<Memory> hierarchy = repository.findAll(new Sort(Sort.Direction.ASC, "index"));
        hierarchy.forEach(memory -> {
            if (memory.parentId != null) {
                hierarchy.stream().filter(other -> memory.parentId.equals(other.id))
                        .findFirst().ifPresent(m -> m.memories.add(memory));
            }
        });
        return hierarchy.stream().filter(memory -> memory.parentId == null).collect(Collectors.toList());
    }

    public List<Memory> getAll() {
        return repository.findAll();
    }

    public Memory get(UUID memoryId) {
        return repository.findOne(memoryId);
    }

    public Memory create(Memory memory) {
        memory.id = UUID.randomUUID();
        memory.index = 0;
        saveMemoryInOrder(memory);
        return memory;
    }

    public Memory update(Memory memory) {
        Memory oldMemory = repository.findOne(memory.id);
        if (oldMemory.parentId != memory.parentId) {
            removeMemoryFromOrder(oldMemory);
            saveMemoryInOrder(memory);
        }
        else if (oldMemory.index != memory.index) {
            saveMemoryInOrder(memory);
        }
        else{
            return repository.save(memory);
        }
        return memory;
    }

    public void delete(UUID memoryId) {
        Memory memory = repository.findOne(memoryId);
        removeMemoryFromOrder(memory);
        repository.delete(memoryId);
    }

    private void saveMemoryInOrder(Memory memory) {
        List<Memory> memories = repository.findAllByParentId(
                memory.parentId, new Sort(Sort.Direction.ASC, "index"));
        memories.remove(memory);
        memories.add(memory.index, memory);
        setOrderIndexes(memories, 0);
        repository.save(memories);
    }

    private void removeMemoryFromOrder(Memory memory) {
        List<Memory> memories = repository.findAllByParentIdAndIndexGreaterThanEqual(
                memory.parentId, memory.index + 1, new Sort(Sort.Direction.ASC, "index"));
        setOrderIndexes(memories, memory.index);
        repository.save(memories);
    }

    private void setOrderIndexes(List<Memory> memories, int start) {
        for (int i = 0; i < memories.size(); i++) {
            memories.get(i).index = i + start;
        }
    }

}
