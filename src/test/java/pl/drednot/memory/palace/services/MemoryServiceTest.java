package pl.drednot.memory.palace.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Sort;
import pl.drednot.memory.palace.data.Memory;
import pl.drednot.memory.palace.data.MemoryRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class MemoryServiceTest {


    @Captor
    private ArgumentCaptor<List<Memory>> memoryListCaptor;
    @Mock
    private MemoryRepository repository;
    private Sort sort = new Sort(Sort.Direction.ASC, "index");

    private MemoryService sut;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        sut = new MemoryService(repository);
    }

    @Test
    public void shouldReturnValidArticleHierarchy() {
        //given
        Memory memory1 = new Memory(UUID.randomUUID(), null);
        Memory memory11 = new Memory(UUID.randomUUID(), memory1.id);
        Memory memory2 = new Memory(UUID.randomUUID(), null);
        Memory memory3 = new Memory(UUID.randomUUID(), null);
        Memory memory33 = new Memory(UUID.randomUUID(), memory3.id);
        Memory memory333 = new Memory(UUID.randomUUID(), memory33.id);
        List<Memory> givenMemories = Arrays.asList(memory1, memory11, memory2, memory3, memory33, memory333);
        when(repository.findAll(sort)).thenReturn(givenMemories);

        //when
        List<Memory> actualHierarchy = sut.getHierarchy();

        //then
        assertThat(actualHierarchy)
                .hasSize(3)
                .contains(memory1, memory2, memory3);

        assertThat(actualHierarchy)
                .filteredOn(article -> article.equals(memory1))
                .flatExtracting(article -> article.memories)
                .hasSize(1)
                .contains(memory11);

        assertThat(actualHierarchy)
                .filteredOn(article -> article.equals(memory3))
                .flatExtracting(article -> article.memories)
                .hasSize(1)
                .contains(memory33)
                .flatExtracting(article -> article.memories)
                .hasSize(1)
                .contains(memory333);
    }

    @Test
    public void shouldChangeOrderIndexesOfMemoriesAfterDeletedMemory() {
        //given
        Memory memoryToDelete = new Memory();
        memoryToDelete.id = UUID.randomUUID();
        memoryToDelete.parentId = UUID.randomUUID();
        memoryToDelete.index = 3;

        Memory firstAfter = new Memory();
        firstAfter.id = UUID.randomUUID();
        firstAfter.index = 4;
        Memory secondAfter = new Memory();
        secondAfter.id = UUID.randomUUID();
        secondAfter.index = 5;
        List<Memory> memoriesAfterMemoryToDelete = Arrays.asList(firstAfter, secondAfter);

        when(repository.findOne(memoryToDelete.id)).thenReturn(memoryToDelete);
        when(repository.findAllByParentIdAndIndexGreaterThanEqual(memoryToDelete.parentId, 4, sort)).thenReturn(memoriesAfterMemoryToDelete);

        //when
        sut.delete(memoryToDelete.id);

        //then
        verify(repository).delete(memoryToDelete.id);
        verify(repository).save(memoryListCaptor.capture());
        List<Memory> savedMemories = memoryListCaptor.getValue();
        assertThat(savedMemories)
                .hasSize(2)
                .contains(firstAfter, secondAfter)
                .extracting(memory -> memory.index)
                .contains(3, 4);
    }

    @Test
    public void shouldChangeOrderIndexesOfMemoriesAfterCreatedMemory() {
        //given
        Memory memoryToCreate = new Memory();
        memoryToCreate.parentId = UUID.randomUUID();

        Memory firstAfter = new Memory();
        firstAfter.id = UUID.randomUUID();
        firstAfter.index = 0;
        Memory secondAfter = new Memory();
        secondAfter.id = UUID.randomUUID();
        secondAfter.index = 1;
        List<Memory> memoriesAfterMemoryToCreate = new ArrayList<>();
        memoriesAfterMemoryToCreate.add(firstAfter);
        memoriesAfterMemoryToCreate.add(secondAfter);

        when(repository.findAllByParentId(memoryToCreate.parentId, sort)).thenReturn(memoriesAfterMemoryToCreate);

        //when
        sut.create(memoryToCreate);

        //then
        verify(repository).save(memoryListCaptor.capture());
        List<Memory> savedMemories = memoryListCaptor.getValue();
        assertThat(savedMemories)
                .hasSize(3)
                .contains(memoryToCreate, firstAfter, secondAfter)
                .extracting(memory -> memory.index)
                .contains(0, 1, 2);
    }

    @Test
    public void shouldChangeOrderIndexesOfMemoriesAfterUpdatedMemory() {
        //given
        Memory oldMemory = new Memory();
        oldMemory.id = UUID.randomUUID();
        oldMemory.index = 2;
        oldMemory.parentId = UUID.randomUUID();

        Memory updatedMemory = new Memory();
        updatedMemory.id = oldMemory.id;
        updatedMemory.parentId = oldMemory.parentId;
        updatedMemory.index = 1;

        Memory first = new Memory();
        first.id = UUID.randomUUID();
        first.index = 0;
        Memory second = new Memory();
        second.id = UUID.randomUUID();
        second.index = 1;
        List<Memory> memories = new ArrayList<>();
        memories.add(first);
        memories.add(second);
        memories.add(oldMemory);

        when(repository.findOne(oldMemory.id)).thenReturn(oldMemory);
        when(repository.findAllByParentId(updatedMemory.parentId, sort)).thenReturn(memories);

        //when
        sut.update(updatedMemory);

        //then
        verify(repository).save(memoryListCaptor.capture());
        List<Memory> savedMemories = memoryListCaptor.getValue();
        assertThat(savedMemories)
                .hasSize(3)
                .contains(first, updatedMemory, second)
                .extracting(memory -> memory.index)
                .contains(0, 1, 2);
    }

    @Test
    public void shouldChangeOrderIndexesOfOldAndNewMemoriesAfterUpdatedMemory() {
        //given
        Memory oldMemory = new Memory();
        oldMemory.id = UUID.randomUUID();
        oldMemory.index = 2;
        oldMemory.parentId = UUID.randomUUID();

        Memory updatedMemory = new Memory();
        updatedMemory.id = oldMemory.id;
        updatedMemory.parentId = UUID.randomUUID();
        updatedMemory.index = 0;

        Memory secondInOldParent = new Memory();
        secondInOldParent.id = UUID.randomUUID();
        secondInOldParent.index = 3;
        List<Memory> memoriesAfterOldMemory = new ArrayList<>(2);
        memoriesAfterOldMemory.add(secondInOldParent);

        Memory firstInNewParent = new Memory();
        firstInNewParent.id = UUID.randomUUID();
        firstInNewParent.index = 0;
        List<Memory> memoriesInNewParent = new ArrayList<>(2);
        memoriesInNewParent.add(firstInNewParent);

        when(repository.findOne(oldMemory.id)).thenReturn(oldMemory);
        when(repository.findAllByParentIdAndIndexGreaterThanEqual(oldMemory.parentId, 3, sort)).thenReturn(memoriesAfterOldMemory);
        when(repository.findAllByParentId(updatedMemory.parentId, sort))
                .thenReturn(memoriesInNewParent);

        //when
        sut.update(updatedMemory);

        //then
        verify(repository, times(2)).save(memoryListCaptor.capture());
        List<List<Memory>> savedMemories = memoryListCaptor.getAllValues();
        assertThat(savedMemories).hasSize(2);
        assertThat(savedMemories.get(0))
                .hasSize(1)
                .contains(secondInOldParent)
                .extracting(memory -> memory.index)
                .contains(2);

        assertThat(savedMemories.get(1))
                .hasSize(2)
                .contains(updatedMemory, firstInNewParent)
                .extracting(memory -> memory.index)
                .contains(0, 1);
    }

}