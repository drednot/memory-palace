import {Component, OnInit, Input} from '@angular/core';
import {ConsoleSection, ListSection, Memory, MemorySection, ParagraphSection} from '../../model/memory.model';
import {MemoriesContext} from '../../model/memories-context.model';
import {MemoriesService} from '../../services/backend/memories-backend.service';
import {ActivatedRoute} from '@angular/router';
import {DeleteSectionObserver} from './editors/delete-section-observer';
import {ElementMoverService} from "../../services/element-mover.service";
import {SaveSectionObserver} from "./editors/save-section-observer";

@Component({
  selector: 'app-memory-detail',
  templateUrl: './memory-detail.component.html',
  styleUrls: ['./memory-detail.component.scss']
})
export class MemoryDetailComponent implements OnInit {
  @Input() memoriesContext: MemoriesContext;
  deleteObserver: DeleteSectionObserver;
  saveObserver: SaveSectionObserver;
  sectionMoving = false;

  constructor(private memoriesService: MemoriesService,
              private elementMover: ElementMoverService,
              private route: ActivatedRoute) {
    const self = this;
    this.deleteObserver = {
      onDelete(section: MemorySection) {
        const index = self.memoriesContext.selected.sections.indexOf(section);
        if (index > -1) {
          self.memoriesContext.selected.sections.splice(index, 1);
        }
      }
    };
    this.saveObserver = {
      onSave(){
        self.save();
      }
    };
    this.observeMemoryId();
  }

  private observeMemoryId() {
    this.route.queryParams.subscribe(params => {
      const memoryId = params['memoryId'];
      if (memoryId != null && this.memoriesContext.selected != null) {
        this.memoriesService.getMemory(memoryId).then(memory => {
          this.memoriesContext.selected.sections = memory.sections;
        });
      }
    });
  }

  ngOnInit() {
  }

  selectSeparator(event: any) {
    event.target.classList.add('selected');
  }

  unSelectSeparator(event: any) {
    event.target.classList.remove('selected');
  }

  startMovingSection(section: MemorySection, event: any) {
    event.dataTransfer.setData('index', this.memoriesContext.selected.sections.indexOf(section));
    this.sectionMoving = true;
  }

  moveSection(targetSection: MemorySection, event: any, beforeTarget: boolean) {
    const sourceIndex = event.dataTransfer.getData('index') as number;
    const sections = this.memoriesContext.selected.sections;
    const sourceSection = sections[sourceIndex];
    this.elementMover.moveElement(
      sourceSection, sections,
      targetSection, sections,
      beforeTarget);
    this.unSelectSeparator(event);
    this.sectionMoving = false;
    event.stopPropagation();
    this.save();
  }

  cancelMovingSection() {
    this.sectionMoving = false;
  }

  allowSectionMoving(event: any) {
    event.preventDefault();
  }

  addParagraph() {
    this.memoriesContext.selected.sections.push(new ParagraphSection());
    this.save();
  }

  addList() {
    this.memoriesContext.selected.sections.push(new ListSection());
    this.save();
  }

  addConsole() {
    this.memoriesContext.selected.sections.push(new ConsoleSection());
    this.save();
  }

  save() {
    this.memoriesService.updateMemory(this.memoriesContext.selected).then(memory => {
      this.memoriesContext.selected.sections = memory.sections;
    });
  }

  delete() {
    this.memoriesService.deleteMemory(this.memoriesContext.selected.id).then(() => {
      this.deleteMemoryFromLocalHierarchy();
    });
  }

  deleteMemoryFromLocalHierarchy() {
    this.deleteMemoryFromMemories(this.memoriesContext.selected, this.memoriesContext.memories);
    this.memoriesContext.selected = null;
  }

  deleteMemoryFromMemories(memory: Memory, memories: Memory[]) {
    const index = memories.indexOf(memory);
    if (index > -1) {
      memories.splice(index, 1);
    } else {
      for (const m of memories) {
        this.deleteMemoryFromMemories(memory, m.memories);
      }
    }
  }

}
