import { Component, OnInit, Input } from '@angular/core';
import {ConsoleSection} from '../../../../model/memory.model';

@Component({
  selector: 'app-console',
  templateUrl: './console.component.html',
  styleUrls: ['./console.component.scss']
})
export class ConsoleComponent implements OnInit {
  @Input() section: ConsoleSection;

  constructor() { }

  ngOnInit() {
  }

}
