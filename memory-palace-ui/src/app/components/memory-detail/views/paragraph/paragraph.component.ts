import { Component, OnInit, Input } from '@angular/core';
import {ParagraphSection} from '../../../../model/memory.model';

@Component({
  selector: 'app-paragraph',
  templateUrl: './paragraph.component.html',
  styleUrls: ['./paragraph.component.scss']
})
export class ParagraphComponent implements OnInit {
  @Input() section: ParagraphSection;

  constructor() { }

  ngOnInit() {
  }

}
