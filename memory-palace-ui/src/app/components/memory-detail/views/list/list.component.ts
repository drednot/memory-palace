import {Component, Input, OnInit} from '@angular/core';
import {ListSection} from '../../../../model/memory.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() section: ListSection;

  constructor() { }

  ngOnInit() {
  }

}
