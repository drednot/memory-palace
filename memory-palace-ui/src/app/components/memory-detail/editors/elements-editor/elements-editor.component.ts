import {Component, OnInit, Input} from '@angular/core';
import {ListElement, ListSection} from '../../../../model/memory.model';
import {DeleteSectionObserver} from '../delete-section-observer';
import {SaveSectionObserver} from "../save-section-observer";

@Component({
  selector: 'app-elements-editor',
  templateUrl: './elements-editor.component.html',
  styleUrls: ['./elements-editor.component.scss']
})
export class ListEditorComponent implements OnInit {
  @Input() section: ListSection;
  @Input() deleteObserver: DeleteSectionObserver;
  @Input() saveObserver: SaveSectionObserver;

  constructor() {
  }

  ngOnInit() {
  }

  save(){
    this.saveObserver.onSave();
  }

  addElement() {
    this.section.elements.push(new ListElement());
    this.save();
  }

  deleteElement(index: number) {
    this.section.elements.splice(index, 1);
    if (this.section.elements.length === 0) {
      this.deleteObserver.onDelete(this.section);
    }
    this.save();
  }

}
