import { Component, OnInit, Input } from '@angular/core';
import {ParagraphSection} from '../../../../model/memory.model';
import {SaveSectionObserver} from "../save-section-observer";

@Component({
  selector: 'app-paragraph-editor',
  templateUrl: './paragraph-editor.component.html',
  styleUrls: ['./paragraph-editor.component.scss']
})
export class ParagraphEditorComponent implements OnInit {
  @Input() section: ParagraphSection;
  @Input() saveObserver: SaveSectionObserver;

  constructor() { }

  ngOnInit() {
  }

  save(){
    this.saveObserver.onSave();
  }

}
