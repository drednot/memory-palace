import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParagraphEditorComponent } from './paragraph-editor.component';

describe('ParagraphEditorComponent', () => {
  let component: ParagraphEditorComponent;
  let fixture: ComponentFixture<ParagraphEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParagraphEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParagraphEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
