import {MemorySection} from '../../../model/memory.model';

export interface DeleteSectionObserver {
  onDelete(section: MemorySection);
}
