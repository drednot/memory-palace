import {Injectable} from '@angular/core';
import {Memory} from '../../model/memory.model';
import {MemoriesService} from '../../services/backend/memories-backend.service';
import {ElementMoverService} from '../../services/element-mover.service';

@Injectable()
export class MemoryNodeMoverService {

  public constructor(private memoriesService: MemoriesService,
                     private elementMover: ElementMoverService) {
  }

  moveMemory(movedMemory: Memory, sourceMemories: Memory[],
             targetMemory: Memory, targetParent: Memory,
             rootMemories: Memory[],
             beforeTarget: boolean) {
    const currentMovedMemoryIndex = sourceMemories.indexOf(movedMemory);
    let targetMemories = rootMemories;
    movedMemory.parentId = null;

    if (this.memoryIsNotMovedToItself(movedMemory, targetParent)) {
      if (targetParent != null) {
        targetMemories = targetParent.memories;
        movedMemory.parentId = targetParent.id;
      }
      if(targetMemory == null){
        movedMemory.index = 0;
      }
      else {
        movedMemory.index = targetMemories.indexOf(targetMemory);
        if (beforeTarget &&
          !this.memoryIsNotMovedFartherInTheSameMemories(sourceMemories, targetMemories,
            movedMemory, currentMovedMemoryIndex)) {
          movedMemory.index--;
        } else if (!beforeTarget &&
          this.memoryIsNotMovedFartherInTheSameMemories(sourceMemories, targetMemories,
            movedMemory, currentMovedMemoryIndex)) {
          movedMemory.index++;
        }
      }
      this.memoriesService.updateMemory(movedMemory).then(() => {
        this.moveMemoryLocally(movedMemory, sourceMemories, targetMemory, targetMemories, beforeTarget);
      });
    }
  }

  private memoryIsNotMovedFartherInTheSameMemories(sourceMemories: Memory[], targetMemories: Memory[],
                                                   movedMemory: Memory, currentMovedMemoryIndex: number) {
    return (sourceMemories !== targetMemories ||
      movedMemory.index <= currentMovedMemoryIndex);
  }

  private moveMemoryLocally(movedMemory: Memory, sourceMemories: Memory[],
                            targetMemory: Memory, targetMemories: Memory[],
                            beforeTarget: boolean) {
    this.elementMover.moveElement(
      movedMemory, sourceMemories,
      targetMemory, targetMemories,
      beforeTarget);
  }

  private memoryIsNotMovedToItself(movedMemory: Memory, targetParent: Memory) {
    return movedMemory !== targetParent;
  }

}
