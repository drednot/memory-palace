import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemoryNodeComponent } from './memory-node.component';

describe('MemoryNodeComponent', () => {
  let component: MemoryNodeComponent;
  let fixture: ComponentFixture<MemoryNodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemoryNodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoryNodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
