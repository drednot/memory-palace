import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Memory} from '../../model/memory.model';
import {MemoriesContext} from '../../model/memories-context.model';
import {MemoryNodeMoverService} from './memory-node-mover.service';

@Component({
  selector: 'app-memory-node',
  templateUrl: './memory-node.component.html',
  styleUrls: ['./memory-node.component.scss']
})
export class MemoryNodeComponent implements OnInit {
  @Input() memory: Memory;
  @Input() parentMemory: Memory;
  @Input() memoriesContext: MemoriesContext;
  sectionMoving = false;

  constructor(private router: Router, private route: ActivatedRoute,
              private memoryNodeMover: MemoryNodeMoverService) {
  }

  ngOnInit() {
  }

  startMovingThisMemory() {
    this.select();
    if (this.parentMemory != null) {
      this.memoriesContext.selectedMemoryBranch = this.parentMemory.memories;
    } else {
      this.memoriesContext.selectedMemoryBranch = this.memoriesContext.memories;
    }
    this.sectionMoving = true;
  }

  moveMemoryNearHere(event: any, beforeTarget: boolean) {
    const sourceMemories = this.memoriesContext.selectedMemoryBranch;
    const movedMemory = this.memoriesContext.selected;

    this.memoryNodeMover.moveMemory(movedMemory, sourceMemories,
      this.memory, this.parentMemory,
      this.memoriesContext.memories, beforeTarget);

    this.unSelectMoveTarget(event);
    this.sectionMoving = false;
    event.stopPropagation();
  }

  moveMemoryInsideHere(event: any){
    const sourceMemories = this.memoriesContext.selectedMemoryBranch;
    const movedMemory = this.memoriesContext.selected;

    let targetMemory = null;
    if(this.memory.memories.length > 0){
      targetMemory = this.memory.memories[this.memory.memories.length - 1];
    }

    this.memoryNodeMover.moveMemory(movedMemory, sourceMemories,
      targetMemory, this.memory,
      this.memoriesContext.memories, false);

    this.unSelectMoveTarget(event);
    this.sectionMoving = false;
    event.stopPropagation();
  }

  cancelMovingThisMemory() {
    this.sectionMoving = false;
  }

  allowToMoveMemoryHere(event: any) {
    event.preventDefault();
  }

  selectMoveTarget(event: any) {
    event.target.classList.add('selected');
  }

  unSelectMoveTarget(event: any) {
    event.target.classList.remove('selected');
  }


  select() {
    this.router.navigate([], {relativeTo: this.route, queryParams: {'memoryId': this.memory.id}});
    if (this.memoriesContext.selected != null) {
      this.memoriesContext.selected.isSelected = false;
    }
    this.memoriesContext.selected = this.memory;
    this.memoriesContext.selected.isSelected = true;
  }

  toggle() {
    if (this.memory.memories.length > 0) {
      this.memory.isCollapsed = !this.memory.isCollapsed;
    }
  }

}
