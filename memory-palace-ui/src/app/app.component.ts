import {Component} from '@angular/core';
import {MemoriesContext} from './model/memories-context.model';
import {MemoriesService} from './services/backend/memories-backend.service';
import {Memory} from './model/memory.model';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  memoriesContext = new MemoriesContext();

  constructor(private memoriesService: MemoriesService,
              private route: ActivatedRoute) {
    memoriesService.getMemories().then(memories => {
      this.memoriesContext.memories = memories;
      const memoryId = this.route.snapshot.queryParamMap.get('memoryId');
      if (memoryId != null) {
        this.memoriesContext.selected = this.selectChosenMemory(memoryId, this.memoriesContext.memories);
        this.memoriesService.getMemory(memoryId).then(memory => {
          this.memoriesContext.selected.sections = memory.sections;
        });
      }
      this.initMemories(memories);
    });
  }

  selectChosenMemory(memoryId: string, memories: Memory[]): Memory {
    for (const memory  of memories) {
      if (memory.id === memoryId) {
        return memory;
      }
      const childMemory = this.selectChosenMemory(memoryId, memory.memories);
      if (childMemory != null) {
        return childMemory;
      }
    }
    return null;
  }

  addMemory() {
    this.memoriesService.createMemory(new Memory('New memory')).then(memory => {
      const newMemories = [memory];
      newMemories.concat(this.memoriesContext.memories);
      this.memoriesContext.memories = newMemories;
      if (this.memoriesContext.selected != null) {
        this.memoriesContext.selected.isSelected = false;
      }
      this.memoriesContext.selected = memory;
      memory.isSelected = true;
    });
  }

  private initMemories(memories: Memory[]) {
    for (const memory of memories) {
      memory.isCollapsed = true;
      this.initMemories(memory.memories);
    }
  }
}
