import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {environment} from '../../../environments/environment';

@Injectable()
export class CommonBackendService {

  constructor(public http: Http) {
  }

  uploadFile(url: string, file: Blob) {
    const formData = new FormData();
    formData.append('attachment', file);
    const options = new RequestOptions({ withCredentials: true });
    return this.http.post(environment.apiUrl + url, formData, options)
      .toPromise().catch(error => this.onError(error));
  }

  postForm(url: string, formData: string) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(environment.apiUrl + url, formData, options)
      .toPromise().catch(error => this.onError(error));
  }

  post(url: string, body?: any) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(environment.apiUrl + url, body , options)
      .toPromise().catch(error => this.onError(error));
  }

  put(url: string, body?: any) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.put(environment.apiUrl + url, body , options)
      .toPromise().catch(error => this.onError(error));
  }

  get(url: string) {
    return this.http.get(environment.apiUrl + url, { withCredentials: true })
      .toPromise().catch(error => this.onError(error));
  }

  delete(url: string) {
    return this.http.delete(environment.apiUrl + url, { withCredentials: true })
      .toPromise().catch(error => this.onError(error));
  }

  onError(error: any) {
    //TODO: handle errors
    return Promise.reject(error.message || error);
  }

}
