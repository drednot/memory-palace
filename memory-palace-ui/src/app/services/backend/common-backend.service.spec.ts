import { TestBed, inject } from '@angular/core/testing';

import { CommonBackendService } from './common-backend.service';

describe('CommonBackendService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommonBackendService]
    });
  });

  it('should be created', inject([CommonBackendService], (service: CommonBackendService) => {
    expect(service).toBeTruthy();
  }));
});
