import {Injectable} from '@angular/core';
import {CommonBackendService} from './common-backend.service';
import {Http} from '@angular/http';
import {Memory} from '../../model/memory.model';

@Injectable()
export class MemoriesService extends CommonBackendService {

  constructor(http: Http) {
    super(http);
  }

  getMemories(){
    return this.get('/memories/hierarchy').then(response => response.json() as Memory[]);
  }

  createMemory(memory: Memory){
    return this.post('/memories', memory).then(response => response.json() as Memory);
  }

  deleteMemory(id: string) {
    return this.delete('/memories/' + id).then();
  }

  getMemory(id: string) {
    return this.get('/memories/' + id).then(response => response.json() as Memory);
  }

  updateMemory(memory: Memory) {
    return this.put('/memories/' + memory.id, memory).then(response => response.json() as Memory);
  }
}
