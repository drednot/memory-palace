import {TestBed, inject} from '@angular/core/testing';

import {ElementMoverService} from './element-mover.service';

describe('ElementMoverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ElementMoverService]
    });
  });

  it('should move first element from source to target after last element', inject([ElementMoverService], (service: ElementMoverService) => {
    // given
    const sourceIndex = 0;
    const sourceElements = ['1', '2', '3'];
    const targetElement = '3';
    const targetElements = ['1', '2', '3'];
    const beforeTarget = false;

    // when
    service.moveElement(sourceIndex, sourceElements, targetElement, targetElements, beforeTarget);

    // then
    expect(targetElements.length).toEqual(4);
  }));

  it('should be created', inject([ElementMoverService], (service: ElementMoverService) => {
    expect(service).toBeTruthy();
  }));
});
