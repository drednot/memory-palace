import {Injectable} from '@angular/core';

@Injectable()
export class ElementMoverService {

  constructor() {
  }

  moveElement(sourceElement: any, sourceElements: any[],
              targetElement: any, targetElements: any[],
              beforeTarget: boolean) {
    const sourceIndex = sourceElements.indexOf(sourceElement);
    sourceElements.splice(sourceIndex, 1);
    let targetIndex = 0;
    if(targetElement != null){
      targetIndex = targetElements.indexOf(targetElement);
      if (!beforeTarget) {
        targetIndex++;
      }
    }
    const elementsAfterTarget: any [] = targetElements.splice(targetIndex, targetElements.length);
    targetElements.push(sourceElement);
    targetElements.push(...elementsAfterTarget);
  }

}
