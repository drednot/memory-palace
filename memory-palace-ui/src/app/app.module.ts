import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';

import {AppComponent} from './app.component';
import {MemoryNodeComponent} from './components/memory-node/memory-node.component';
import {MemoryDetailComponent} from './components/memory-detail/memory-detail.component';
import {MemoriesService} from './services/backend/memories-backend.service';
import {ParagraphEditorComponent} from './components/memory-detail/editors/paragraph-editor/paragraph-editor.component';
import {ListEditorComponent} from './components/memory-detail/editors/elements-editor/elements-editor.component';
import {ParagraphComponent} from './components/memory-detail/views/paragraph/paragraph.component';
import {ListComponent} from './components/memory-detail/views/list/list.component';
import {ConsoleComponent} from './components/memory-detail/views/console/console.component';
import {ElementMoverService} from './services/element-mover.service';
import {MemoryNodeMoverService} from "./components/memory-node/memory-node-mover.service";

const appRoutes: Routes = [
  {path: '', component: AppComponent },
  {path: '**', redirectTo: '', pathMatch: 'full'},
];

@NgModule({
  declarations: [
    AppComponent,
    MemoryNodeComponent,
    MemoryDetailComponent,
    ParagraphEditorComponent,
    ListEditorComponent,
    ParagraphComponent,
    ListComponent,
    ConsoleComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [MemoriesService, ElementMoverService, MemoryNodeMoverService],
  bootstrap: [AppComponent]
})
export class AppModule { }
