import {Memory} from './memory.model';

export class MemoriesContext {
  selected: Memory;
  selectedMemoryBranch: Memory[];
  memories: Memory[];
}
