export class Memory {
  id: string;
  name: string;
  index: number;
  parentId: string;
  memories: Memory[] = [];
  sections: MemorySection[] = [];

  isSelected = false;
  isCollapsed = false;

  constructor(name: string) {
    this.name = name;
  }
}

export class MemorySection {
  constructor(private type: string) {

  }
}

export class ParagraphSection extends MemorySection {
  value: string;

  constructor() {
    super('paragraph');
  }
}

export class ListElement {
  value: string;
}

export class ElementsSection extends MemorySection {
  elements = [new ListElement()];

  constructor(type: string) {
    super(type);
  }
}

export class ConsoleSection extends ElementsSection {

  constructor() {
    super('console');
  }
}

export class ListSection extends ElementsSection {

  constructor() {
    super('list');
  }
}
